package edu.mit.blocks.workspace;

import java.awt.Desktop;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.ResourceBundle;

import edu.mit.blocks.renderable.RenderableBlock;


/**
 * ContextMenu handles all the right-click menus within the Workspace.
 * TODO ria enable customization of what menu items appear, fire events depending
 * on what items are clicked (if we enabled the first feature)
 * 
 * TODO ria still haven't enabled the right click menu for blocks
 */
public class ContextMenu extends PopupMenu implements ActionListener {

    private static final long serialVersionUID = 328149080421L;
    //context menu renderableblocks plus
    //menu items for renderableblock context menu
    private static ContextMenu rndBlockMenu = new ContextMenu();
    private static ContextMenu addCommentMenu = new ContextMenu();
    private static MenuItem addCommentItem;
    private final static String ADD_COMMENT_BLOCK = "ADDCOMMENT";
    private static boolean addCommentMenuInit = false;
    private static ContextMenu removeCommentMenu = new ContextMenu();
    private static MenuItem removeCommentItem;
    private final static String REMOVE_COMMENT_BLOCK = "REMOVECOMMENT";
    private static boolean removeCommentMenuInit = false;
    private final static String CLONE_BLOCK = "CLONE";	//heqichen
    private final static String CLONE_BLOCKS = "CLONEALL"; //letsgoING
    private final static String OPEN_REFERENCE = "OPEN_REFERENCE";
    private static MenuItem cloneItem1 = null;	    //letsgoING
    private static MenuItem cloneItem2 = null;	    //letsgoING
    private static MenuItem cloneAllItem1 = null;	//letsgoING
    private static MenuItem cloneAllItem2 = null;	//letsgoING
    private static MenuItem refrenceItem = null;   //letsgoING
    //context menu for canvas plus
    //menu items for canvas context menu
    private static ContextMenu canvasMenu = new ContextMenu();
    private static MenuItem arrangeAllBlocks;
    
    private final static String ARRANGE_ALL_BLOCKS = "ARRANGE_ALL_BLOCKS";
    private static boolean canvasMenuInit = false;
    /** The JComponent that launched the context menu in the first place */
    private static Object activeComponent = null;

    //privatize the constructor
    private ContextMenu() {
    }

    /**
     * Initializes the context menu for adding Comments.
     */
    private static void initAddCommentMenu() {
    	ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
    	
    	addCommentItem = new MenuItem(uiMessageBundle.getString("ardublock.ui.add_comment"));
        addCommentItem.setActionCommand(ADD_COMMENT_BLOCK);
        addCommentItem.addActionListener(rndBlockMenu);
        addCommentMenu.add(addCommentItem);
        
    	cloneItem1 = new MenuItem(uiMessageBundle.getString("ardublock.ui.clone"));
    	cloneItem1.setActionCommand(CLONE_BLOCK);
    	cloneItem1.addActionListener(rndBlockMenu);
        addCommentMenu.add(cloneItem1);
        
        cloneAllItem1 = new MenuItem(uiMessageBundle.getString("ardublock.ui.clone_all"));
    	cloneAllItem1.setActionCommand(CLONE_BLOCKS);
    	cloneAllItem1.addActionListener(rndBlockMenu);
        addCommentMenu.add(cloneAllItem1);
        
        refrenceItem = new MenuItem(uiMessageBundle.getString("ardublock.ui.reference"));
        refrenceItem.setActionCommand(OPEN_REFERENCE);
        refrenceItem.addActionListener(rndBlockMenu);
        addCommentMenu.add(refrenceItem);
        
        addCommentMenuInit = true;
        
    }

    /**
     * Initializes the context menu for deleting Comments.
     */
    private static void initRemoveCommentMenu() {
    	ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
    	
        removeCommentItem = new MenuItem(uiMessageBundle.getString("ardublock.ui.delete_comment"));
        removeCommentItem.setActionCommand(REMOVE_COMMENT_BLOCK);
        removeCommentItem.addActionListener(rndBlockMenu);

        removeCommentMenu.add(removeCommentItem);
        //rndBlockMenu.add(runBlockItem);
        
    	
    	cloneItem2 = new MenuItem(uiMessageBundle.getString("ardublock.ui.clone"));
    	cloneItem2.setActionCommand(CLONE_BLOCK);
    	cloneItem2.addActionListener(rndBlockMenu);
        removeCommentMenu.add(cloneItem2);
        
        cloneAllItem2 = new MenuItem(uiMessageBundle.getString("ardublock.ui.clone_all"));
    	cloneAllItem2.setActionCommand(CLONE_BLOCKS);
    	cloneAllItem2.addActionListener(rndBlockMenu);
    	removeCommentMenu.add(cloneAllItem2);
    	
        refrenceItem = new MenuItem(uiMessageBundle.getString("ardublock.ui.reference"));
        refrenceItem.setActionCommand(OPEN_REFERENCE);
        refrenceItem.addActionListener(rndBlockMenu);
        removeCommentMenu.add(refrenceItem);
        
        removeCommentMenuInit = true;
    }

    /**
     * Initializes the context menu for the BlockCanvas
     *
     */
    private static void initCanvasMenu() {
    	ResourceBundle uiMessageBundle = ResourceBundle.getBundle("com/ardublock/block/ardublock");
    	
        arrangeAllBlocks = new MenuItem(uiMessageBundle.getString("ardublock.ui.organize_blocks"));  //TODO some workspaces don't have pages
        arrangeAllBlocks.setActionCommand(ARRANGE_ALL_BLOCKS);
        arrangeAllBlocks.addActionListener(canvasMenu);

        canvasMenu.add(arrangeAllBlocks);
        
        canvasMenuInit = true;
    }

    /**
     * Returns the right click context menu for the specified JComponent.  If there is 
     * none, returns null.
     * @param o JComponent object seeking context menu
     * @return the right click context menu for the specified JComponent.  If there is 
     * none, returns null.
     */
    public static PopupMenu getContextMenuFor(Object o) {
        if (o instanceof RenderableBlock) {
            if (((RenderableBlock) o).hasComment()) {
                if (!removeCommentMenuInit) {
                    initRemoveCommentMenu();
                }
                activeComponent = o;
                return removeCommentMenu;
            } else {
                if (!addCommentMenuInit) {
                    initAddCommentMenu();
                }
                activeComponent = o;
                return addCommentMenu;
            }
        } else if (o instanceof BlockCanvas) {
            if (!canvasMenuInit) {
                initCanvasMenu();
            }
            activeComponent = o;
            return canvasMenu;
        }
        return null;
    }
    
    /**
     * opens reference-file (html) for active block 
     * in standard browser
     * @param blockGenusName = genus name of the active block
     * added by letsgoING 
     * @throws URISyntaxException 
     */
    private void createReferenceWindow(String blockGenusName) {
    	File cssFile = null;
    	//File indexFile = null;
    	File htmlFile = null;
    	File imageFile = null;
    	File logoFile = null;
    	File exampleFile  = null;
    	File example2File  = null;
    	
    	String cssResource      ="/com/ardublock/reference/_seitenformatierung.css";
    	//String indexResource      ="/com/ardublock/reference/_inhaltsverzeichnis.html";
    	String htmlResource     ="/com/ardublock/reference/"+blockGenusName+".html";
    	String logoResource     ="/com/ardublock/reference/_Logo_LGI_page.png";
    	String imageResource    ="/com/ardublock/reference/"+blockGenusName+".png";
    	//String exampleResource  ="/com/ardublock/reference/"+blockGenusName+"-beispiel.png";
    	//String example2Resource ="/com/ardublock/reference/"+blockGenusName+"-beispiel-2.png";
    	String example2Resource ="/com/ardublock/reference/"+blockGenusName+"-beispiel-";
    	String exampleFileType  = ".png";
    	//String fileNotFoundResource = "/com/ardublock/reference/404.png";
    	String tempPath = "";
    	
    	/*
    	System.out.println("REMOVE PRINT in ContextMenue.java after BlockRef is done");
    	System.out.println("Blockname HTML: "+ blockGenusName + ".html");
    	System.out.println("Blockname Bild: "+ blockGenusName + ".png");
    	System.out.println("Blockname Bild: "+ blockGenusName + "-beispiel.png");
    	*/
    	
    	//get current .jar path for temp files
    	try {
			tempPath = new File(URLDecoder.decode(getClass().getProtectionDomain().getCodeSource().getLocation().getFile(), "UTF-8")).getParentFile().getPath();
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	
    	//copy css File to tmp files
        try {
            InputStream input = getClass().getResourceAsStream(cssResource);
        	if(input != null) {
	            cssFile = new File(tempPath +File.separatorChar+ "_seitenformatierung.css");
	            OutputStream out = new FileOutputStream(cssFile);
	            int read;
	            byte[] bytes = new byte[1024];
	    	        while ((read = input.read(bytes)) != -1) {
	   	            out.write(bytes, 0, read);
	   	        }
	   	        out.close();
	   	        cssFile.deleteOnExit();
        	} else {
        		//TODO: use resources
        		System.out.println("Sorry, css stylesheet was not found in reference.");
        		//TODO: open 404-page
        		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        	}
   	    } catch (IOException ex) {
   	        ex.printStackTrace();
   	    }
    	
    	//copy html to tmp files
        try {
            InputStream input = getClass().getResourceAsStream(htmlResource);
        	if(input != null) {
	            htmlFile = new File(tempPath +File.separatorChar+ blockGenusName + ".html");
	            OutputStream out = new FileOutputStream(htmlFile);
	            int read;
	            byte[] bytes = new byte[1024];
	    	        while ((read = input.read(bytes)) != -1) {
	   	            out.write(bytes, 0, read);
	   	        }
	   	        out.close();
	   	        htmlFile.deleteOnExit();
        	} else {
        		//TODO: use resources
        		System.out.println("Sorry, block "+blockGenusName+" was not found in reference.");
        		//TODO: open 404-page
        		//!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        	}
   	    } catch (IOException ex) {
   	        ex.printStackTrace();
   	    }
        
      //copy logo to tmp files
   	    try {
   	        InputStream input = getClass().getResourceAsStream(logoResource);
	   	     if(input != null) {
	   	        logoFile = new File(tempPath +File.separatorChar+"_Logo_LGI_page.png");
	   	        OutputStream out = new FileOutputStream(logoFile);
	   	        int read;
	   	        byte[] bytes = new byte[10000];
	    	        while ((read = input.read(bytes)) != -1) {
	   	            out.write(bytes, 0, read);
	   	        }
	   	        out.close();
	   	     logoFile.deleteOnExit();
	   	     } else {
	        		//TODO: use resources
	        		System.out.println("Sorry, logo image was not found.");
	        	}
   	    } catch (IOException ex) {
   	        ex.printStackTrace();
   	    }
        
        //copy block-image to tmp files
   	    try {
   	        InputStream input = getClass().getResourceAsStream(imageResource);
	   	     if(input != null) {
	   	        imageFile = new File(tempPath +File.separatorChar+ blockGenusName+".png");
	   	        OutputStream out = new FileOutputStream(imageFile);
	   	        int read;
	   	        byte[] bytes = new byte[10000];
	    	        while ((read = input.read(bytes)) != -1) {
	   	            out.write(bytes, 0, read);
	   	        }
	   	        out.close();
	   	        imageFile.deleteOnExit();
	   	     } else {
	        		//TODO: use resources
	   	    	System.out.println("Sorry, block image for "+blockGenusName+" was not found in reference.");
	        	}
   	    } catch (IOException ex) {
   	        ex.printStackTrace();
   	    }
   	    
   	    //copy example-image to tmp files
   	/*    try {
   	        InputStream input = getClass().getResourceAsStream(exampleResource);
	   	     if(input != null) {
	   	        exampleFile = new File(tempPath +File.separatorChar+ blockGenusName+"-beispiel.png");
	   	        OutputStream out = new FileOutputStream(exampleFile);
	   	        int read;
	   	        byte[] bytes = new byte[10000];
	    	        while ((read = input.read(bytes)) != -1) {
	   	            out.write(bytes, 0, read);
	   	        }
	   	        out.close();
	   	        exampleFile.deleteOnExit();
	   	     } else {
	        		//TODO: use resources
	        		System.out.println("Sorry, example for "+blockGenusName+" was not found in reference.");
	        	}
   	    } catch (IOException ex) {
   	        ex.printStackTrace();
   	    }*/
   	    
   	    //copy optional second example-image to tmp files
   	    //TODO: add number to all exampleImages -> starting with 1
   	    //TODO: or use just one example image
   	    try {
   	    	boolean allFilesRead = false;
   	    	int exampleImageCounter = 1;
   	    	
   	    	while(!allFilesRead) {
	   	        InputStream input = getClass().getResourceAsStream(example2Resource + exampleImageCounter + exampleFileType);
		   	     if(input != null) {
		   	        example2File = new File(tempPath +File.separatorChar+ blockGenusName+"-beispiel-"+exampleImageCounter+".png");
		   	        OutputStream out = new FileOutputStream(example2File);
		   	        int read;
		   	        byte[] bytes = new byte[10000];
		    	        while ((read = input.read(bytes)) != -1) {
		   	            out.write(bytes, 0, read);
		   	        }
		   	        out.close();
		   	        example2File.deleteOnExit();
		   	        exampleImageCounter++;
		   	     } else {
		   	    	allFilesRead = true;
		        	//TODO: use resources
		        	//System.out.println("Sorry, block was not found in reference.");
		        	}
   	    	}
   	    } catch (IOException ex) {
   	        ex.printStackTrace();
   	    }
   	
   	    //open file in standard browser
    	if (htmlFile != null && !htmlFile.exists() && imageFile != null && !imageFile.exists() && !logoFile.exists() && !exampleFile.exists()) {
    	    throw new RuntimeException("Error: Resource not found!");
    	}else {    	
	    	try {
				Desktop.getDesktop().browse(htmlFile.toURI());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			} catch (NullPointerException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
    	}
        return;
    }

    public void actionPerformed(ActionEvent a) {
        if (a.getActionCommand() == ARRANGE_ALL_BLOCKS) {
            //notify the component that launched the context menu in the first place
            if (activeComponent != null && activeComponent instanceof BlockCanvas) {
                ((BlockCanvas) activeComponent).arrangeAllBlocks();
            }
        } else if (a.getActionCommand() == ADD_COMMENT_BLOCK) {
            //notify the renderableblock componenet that lauched the conetxt menu
            if (activeComponent != null && activeComponent instanceof RenderableBlock) {
                ((RenderableBlock) activeComponent).addComment();
            }
        } else if (a.getActionCommand() == REMOVE_COMMENT_BLOCK) {
            //notify the renderableblock componenet that lauched the conetxt menu
            if (activeComponent != null && activeComponent instanceof RenderableBlock) {
                ((RenderableBlock) activeComponent).removeComment();
            }
        } else if (a.getActionCommand() == CLONE_BLOCKS) {
        	//notify the renderableblock componenet that lauched the conetxt menu
            if (activeComponent != null && activeComponent instanceof RenderableBlock) {
                ((RenderableBlock) activeComponent).cloneMe(true);
            }
        }
        else if (a.getActionCommand() == CLONE_BLOCK) {
        	//notify the renderableblock componenet that lauched the conetxt menu
            if (activeComponent != null && activeComponent instanceof RenderableBlock) {
                ((RenderableBlock) activeComponent).cloneMe(false);
            }
        }
        else if (a.getActionCommand() == OPEN_REFERENCE) {
        	//notify the renderableblock componenet that lauched the conetxt menu
            if (activeComponent != null && activeComponent instanceof RenderableBlock) {
            	createReferenceWindow(((RenderableBlock) activeComponent).getBlock().getGenusName());
            }
        }
    }
}
